<?php
/**
 * @file
 * Expose the task schedule feature to hostmaster.
 */

/**
 * Implements hook_hosting_feature().
 */
function hosting_task_schedule_hosting_feature() {
  $features['task_schedule'] = array(
    'title' => t('Task Schedules'),
    'description' => t('Configure tasks to run on a schedule.'),
    'group' => 'optional',
    'status' => HOSTING_FEATURE_REQUIRED,
    'module' => 'hosting_task_schedule',
  );
  return $features;
}
